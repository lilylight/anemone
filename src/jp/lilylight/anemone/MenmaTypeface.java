/*
 * Cuko-plugin for twicca
 * $Id: MenmaTypeface.java,v 1.0 2012/07/24 18:46:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone;

import android.content.res.AssetManager;
import android.graphics.Typeface;

public class MenmaTypeface {

    /**
     * @see <a href="http://www.anohana.jp/special/font.html">めんまフォント</a>
     */
    public static Typeface create(AssetManager mgr) {
        return Typeface.createFromAsset(mgr, "fonts/menma.ttf");
    }

}
