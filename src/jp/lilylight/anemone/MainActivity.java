/*
 * Cuko-plugin for twicca
 * $Id: MainActivity.java,v 1.4 2012/10/24 21:17:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone;

import java.util.regex.Pattern;

import jp.lilylight.amaryllis.Kakasi;
import jp.lilylight.amaryllis.LogD;
import jp.lilylight.anemone.type.DisplayType;
import jp.lilylight.anemone.type.TweetType;
import jp.lilylight.anemone.util.SimpleAsyncTaskLoader;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;

public class MainActivity extends FragmentActivity implements LoaderCallbacks<String> {

    private SharedPreferences mPreferences;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        getSupportLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<String> onCreateLoader(int id, Bundle args) {
        return new SimpleAsyncTaskLoader<String>(this) {
            @Override
            public String loadInBackground() {
                String action = getIntent().getAction();

                // twicca (ツイート編集)
                if ("jp.r246.twicca.ACTION_EDIT_TWEET".equals(action)) {
                    actionEditTweet();
                }
                // twicca (ツイート表示)
                else if ("jp.r246.twicca.ACTION_SHOW_TWEET".equals(action)) {
                    actionShowTweet();
                }
                // Simeji (マッシュルーム)
                else if ("com.adamrocker.android.simeji.ACTION_INTERCEPT".equals(action)) {
                    actionIntercept();
                }
                // テキスト共有
                else if (Intent.ACTION_SEND.equals(action)) {
                    actionSend();
                }
                return action;
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<String> loader, String data) {
        finish();
    }

    @Override
    public void onLoaderReset(Loader<String> loader) {
    }

    private void actionEditTweet() {
        Bundle extra = getIntent().getExtras();
        LogD.d("text:" + extra.getString(Intent.EXTRA_TEXT));
        LogD.d("latitude:" + extra.getString("latitude"));
        LogD.d("longitude:" + extra.getString("longitude"));
        LogD.d("status_id:" + extra.getString("in_reply_to_status_id"));
        LogD.d("prefix:" + extra.getString("prefix"));
        LogD.d("suffix:" + extra.getString("suffix"));
        LogD.d("user_input:" + extra.getString("user_input"));
        LogD.d("cursor:" + extra.getInt("cursor", -1));

        String text = extra.getString("user_input");

        int id = mPreferences.getInt(DisplayType.TWEET.getKey(), TweetType.TWEET.getId());
        switch (TweetType.idOf(id)) {
            case TWEET:
                Intent intent = new Intent();
                intent.putExtra(Intent.EXTRA_TEXT, getCukoString(text));
                setResult(RESULT_OK, intent);
                break;

            case COPY:
                actionCopy(getCukoString(text));
                break;

            case SHARE:
                actionSend();
                break;
        }
    }

    private void actionShowTweet() {
        Bundle extra = getIntent().getExtras();
        LogD.d("text:" + extra.getString(Intent.EXTRA_TEXT));
        LogD.d("id:" + extra.getString("id"));
        LogD.d("latitude:" + extra.getString("latitude"));
        LogD.d("longitude:" + extra.getString("longitude"));
        LogD.d("created_at:" + extra.getString("created_at"));
        LogD.d("source:" + extra.getString("source"));
        LogD.d("status_id:" + extra.getString("in_reply_to_status_id"));
        LogD.d("user_screen_name:" + extra.getString("user_screen_name"));
        LogD.d("user_name:" + extra.getString("user_name"));
        LogD.d("user_id:" + extra.getString("user_id"));
        LogD.d("user_profile_image_url:" + extra.getString("user_profile_image_url"));
        LogD.d("user_profile_image_url_mini:" + extra.getString("user_profile_image_url_mini"));
        LogD.d("user_profile_image_url_normal:" + extra.getString("user_profile_image_url_normal"));
        LogD.d("user_profile_image_url_bigger:" + extra.getString("user_profile_image_url_bigger"));

        String text = extra.getString(Intent.EXTRA_TEXT);

        int id = mPreferences.getInt(DisplayType.TIMELINE.getKey(), TweetType.TWEET.getId());
        switch (TweetType.idOf(id)) {
            case TWEET:
                tweet(getApplicationContext(), getCukoString(text));
                break;

            case COPY:
                actionCopy(getCukoString(text));
                break;

            case SHARE:
                actionSend();
                break;
        }
    }

    private void actionIntercept() {
        Bundle extra = getIntent().getExtras();
        LogD.d("text:" + extra.getString("replace_key"));

        String text = extra.getString("replace_key");
        Intent intent = new Intent();
        intent.putExtra("replace_key", getCukoString(text));
        setResult(RESULT_OK, intent);
    }

    private void actionSend() {
        String text = getIntent().getStringExtra(Intent.EXTRA_TEXT);
        LogD.d("text:" + text);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.putExtra(Intent.EXTRA_TEXT, isCukoFormat(text) ? text : getCukoString(text));
        intent.setType("text/plain");
        intent = Intent.createChooser(intent, getString(R.string.cuko_share));
        startActivity(intent);
    }

    private void actionCopy(final String text) {
        new Handler(getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                CukoApplication.copyText(text);
            }
        });
    }

    private boolean isCukoFormat(String string) {
        return Pattern.compile(getString(R.string.cuko_pattern)).matcher(string).find();
    }

    private static void tweet(Context context, String tweet) {
        Uri.Builder builder = Uri.parse("https://twitter.com/intent/tweet").buildUpon();
        builder.appendQueryParameter("text", tweet); // WebIntentのパラメータ

        Uri web_intent_uri = builder.build();

        try {
            // twiccaのツイート画面を呼び出し
            Intent intent = new Intent(Intent.ACTION_VIEW, web_intent_uri);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setPackage("jp.r246.twicca");
            context.startActivity(intent);
            return;
        } catch (ActivityNotFoundException e) {
            // twiccaがインストールされていない

            // ブラウザでWebIntentを呼び出し
            Intent intent = new Intent(Intent.ACTION_VIEW, web_intent_uri);
            intent.addCategory(Intent.CATEGORY_DEFAULT);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            return;
        }
    }

    private String getKakasiString(String text) {
        try {
            Kakasi kakasi = new Kakasi();
            kakasi.setupKanjiConverter(Kakasi.HIRAGANA);
            kakasi.setupKatakanaConverter(Kakasi.HIRAGANA);
            return kakasi.doString(text);
        } catch (Exception e) {
            return text;
        }
    }

    private String getCukoString(String text) {
        CukoConverter c2 = new CukoConverter();
        String cstr = c2.doString(text);
        cstr = getKakasiString(cstr);
        cstr = getString(R.string.cuko_format, text, cstr);
        cstr = addHashTag(cstr);
        return cstr;
    }

    private String addHashTag(String text) {
        if (mPreferences.getBoolean(DisplayType.HASHTAG.getKey(), false)) {
            text += " " + getString(R.string.hash_tag);
        }
        return text;
    }

}
