/*
 * Cuko-plugin for twicca
 * $Id: ResourceListAdapter.java,v 1.0 2013/06/10 15:16:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.util;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public abstract class ResourceListAdapter<Item> extends BaseAdapter {

    private Context mContext;
    private int mLayout;
    private int mDropDownLayout;
    private List<Item> mList;

    private boolean mDataValid = false;

    private LayoutInflater mInflater;

    public ResourceListAdapter(Context context, int layout, List<Item> list) {
        mContext = context;
        mLayout = mDropDownLayout = layout;
        mList = list;
        mDataValid = mList != null;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Context getContext() {
        return mContext;
    }

    @Override
    public int getCount() {
        if (mDataValid) {
            return mList.size();
        } else {
            return 0;
        }
    }

    @Override
    public Item getItem(int position) {
        if (mDataValid) {
            return mList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public long getItemId(int position) {
        if (mDataValid) {
            return mList.get(position).hashCode();
        } else {
            return 0;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the list is valid");
        }
        View v;
        if (convertView == null) {
            v = newView(position, mContext, mList.get(position), parent);
        } else {
            v = convertView;
        }
        bindView(position, v, mContext, mList.get(position));
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        if (mDataValid) {
            View v;
            if (convertView == null) {
                v = newDropDownView(position, mContext, mList.get(position), parent);
            } else {
                v = convertView;
            }
            bindView(position, v, mContext, mList.get(position));
            return v;
        } else {
            return null;
        }
    }

    public View newView(int position, Context context, Item item, ViewGroup parent) {
        return mInflater.inflate(mLayout, parent, false);
    }

    public View newDropDownView(int position, Context context, Item item, ViewGroup parent) {
        return mInflater.inflate(mDropDownLayout, parent, false);
    }

    public abstract void bindView(int position, View view, Context context, Item item);

    public void changeList(List<Item> list) {
        if (mList == list) {
            return;
        }
        if (mList != null) {
            mList.clear();
        }
        mList = list;
        if (list != null) {
            mDataValid = true;
            notifyDataSetChanged();
        } else {
            mDataValid = false;
            notifyDataSetInvalidated();
        }
    }
}
