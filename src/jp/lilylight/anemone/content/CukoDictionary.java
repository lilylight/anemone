/*
 * Cuko-plugin for twicca
 * $Id: CukoDictionary.java,v 1.0 2013/05/30 17:52:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.content;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class CukoDictionary {

    private static final List<Word> sDictionary =
            Collections.synchronizedList(new ArrayList<Word>());

    /**
     * Reads and adds dictionary entries from the file. The file encoding is
     * "UTF-8".
     * 
     * @param filename the file name.
     * @exception IOException if an error occurred when opening or reading the
     *                file.
     */
    public void load(String filename) throws IOException {
        load(filename, "UTF-8");
    }

    /**
     * Reads and adds dictionary entries from the file with the specified
     * encoding.
     * 
     * @param filename the file name.
     * @param encoding the file encoding.
     * @exception IOException if an error occurred when opening or reading the
     *                file.
     */
    public void load(String filename, String encoding) throws IOException {
        File file = new File(filename);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
            file.createNewFile();
            return;
        }

        InputStream in = new FileInputStream(file);
        try {
            Reader reader = new InputStreamReader(in, encoding);
            try {
                load(reader);
            } finally {
                try {
                    reader.close();
                    in = null;
                } catch (IOException e) {
                }
            }
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * Reads and adds dictionary entries from the reader.
     * 
     * @param reader the reader object.
     * @exception IOException if an error occurred when reading from the reader.
     */
    public void load(Reader reader) throws IOException {
        BufferedReader in = new BufferedReader(reader);
        while (true) {
            String line = in.readLine();
            if (line == null) {
                break;
            }
            int length = line.length();
            if (length == 0) {
                continue;
            }
            if (line.startsWith(";;")) {
                continue;
            }
            String[] item = line.split(" ");
            String yomi = item[0];
            String tango = item[1];
            sDictionary.add(new Word(tango, yomi));
        }
    }

    /**
     * Saves this Cuko dictionary to the specified file. The file encoding is
     * "UTF-8".
     * 
     * @param filename the destination file.
     * @exception IOException if an error occurred when writing to the file.
     */
    public void save(String filename) throws IOException {
        save(filename, "UTF-8");
    }

    /**
     * Saves this Cuko dictionary to the specified file.
     * 
     * @param filename the destination file.
     * @param encoding the file encoding.
     * @exception IOException if an error occurred when writing to the file.
     */
    public void save(String filename, String encoding) throws IOException {
        File file = new File(filename);
        file.getParentFile().mkdirs();

        OutputStream out = new FileOutputStream(file);
        try {
            Writer writer = new OutputStreamWriter(out, encoding);
            try {
                save(writer);
            } finally {
                try {
                    writer.close();
                    out = null;
                } catch (IOException e) {
                }
            }
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                }
            }
        }
    }

    /**
     * Saves this Cuko dictionary to the specified file.
     * 
     * @param writer the writer object.
     * @exception IOException if an error occurred when writing to the file.
     */
    public void save(Writer writer) throws IOException {
        BufferedWriter out = new BufferedWriter(writer);
        StringBuilder sb = new StringBuilder();
        int size = sDictionary.size();
        for (int i = 0; i < size; i++) {
            Word word = sDictionary.get(i);
            sb.append(word.getYomi())
              .append(" ")
              .append(word.getTango())
              .append("\n");
        }
        out.write(sb.toString());
        out.flush();
    }

    /**
     * Add dictionary entry.
     * 
     * @param word the Word object.
     */
    public void addWord(Word word) {
        sDictionary.add(word);
    }

    /**
     * Add dictionary entry.
     * 
     * @param location the index at which to insert.
     * @param word the Word object.
     */
    public void addWord(int location, Word word) {
        sDictionary.add(location, word);
    }

    /**
     * Set dictionary entry.
     * 
     * @param location the index at which to insert.
     * @param word the Word object.
     */
    public void setWord(int location, Word word) {
        sDictionary.set(location, word);
    }

    /**
     * Get dictionary entry.
     * 
     * @param location the index of the word to return.
     */
    public Word getWord(int location) {
        return sDictionary.get(location);
    }

    /**
     * Gets all dictionary entry.
     * 
     * @return the word list.
     */
    public List<Word> getWordList() {
        return sDictionary;
    }

    /**
     * Remove dictionary entry.
     * 
     * @param word the Word object.
     */
    public void removeWord(Word word) {
        sDictionary.remove(word);
    }

    /**
     * Remove dictionary entry.
     * 
     * @param location the index of the word to remove.
     * @return the removed word.
     */
    public Word removeWord(int location) {
        return sDictionary.remove(location);
    }

    /**
     * Searches this Dictionary for the specified tango and returns the index of
     * the first occurrence.
     * 
     * @param tango the tango string.
     * @return the index of the first occurrence of the tango or -1 if the tango
     *         was not found.
     */
    public int indexOf(String tango) {
        int size = sDictionary.size();
        for (int i = 0; i < size; i++) {
            Word word = sDictionary.get(i);
            if (word.getTango().equals(tango)) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Returns whether this dictionary is empty
     * 
     * @return true if this dictionary has no elements, false otherwise.
     */
    public boolean isEmpty() {
        return sDictionary.isEmpty();
    }
}
