/*
 * Cuko-plugin for twicca
 * $Id: Word.java,v 1.0 2013/06/10 15:15:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.content;

import java.util.Map.Entry;

public class Word implements Entry<String, String> {
    private String mTango;
    private String mYomi;

    public Word() {
        this(null, null);
    }

    public Word(String tango, String yomi) {
        mTango = tango;
        mYomi = yomi;
    }

    @Override
    public String getKey() {
        return getTango();
    }

    @Override
    public String getValue() {
        return getYomi();
    }

    @Override
    public String setValue(String object) {
        setYomi(object);
        return object;
    }

    public String getTango() {
        return mTango;
    }

    public void setTango(String tango) {
        mTango = tango;
    }

    public String getYomi() {
        return mYomi;
    }

    public void setYomi(String yomi) {
        mYomi = yomi;
    }

}
