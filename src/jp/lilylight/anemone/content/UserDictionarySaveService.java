/*
 * Cuko-plugin for twicca
 * $Id: UserDictionarySaveService.java,v 1.0 2013/06/18 19:08:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.content;

import static jp.lilylight.anemone.CukoApplication.getDictionary;
import static jp.lilylight.anemone.CukoApplication.getDictionaryName;

import java.io.IOException;

import jp.lilylight.amaryllis.LogD;
import android.app.IntentService;
import android.content.Intent;

public class UserDictionarySaveService extends IntentService {

    public UserDictionarySaveService() {
        this("UserDictionarySaveService");
    }

    public UserDictionarySaveService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            getDictionary().save(getDictionaryName());
            LogD.i("Dictionary save success.");
        } catch (IOException e) {
            LogD.w("Dictionary save failed.");
            e.printStackTrace();
        }
    }

}
