/*
 * Cuko-plugin for twicca
 * $Id: UserDictionaryToolActivity.java,v 1.0 2013/06/10 17:48:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.content;

import java.util.regex.Pattern;

import jp.lilylight.anemone.CukoApplication;
import jp.lilylight.anemone.R;
import jp.lilylight.anemone.content.UserDictionaryToolFragment.Action;
import jp.lilylight.anemone.content.UserDictionaryToolFragment.UserDictionaryToolListener;
import jp.lilylight.anemone.type.ActionType;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

public class UserDictionaryToolActivity extends SherlockFragmentActivity
        implements UserDictionaryToolListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.Theme_Sherlock_Light);
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            return;
        }

        FragmentManager fm = getSupportFragmentManager();

        if (fm.findFragmentById(android.R.id.content) == null) {
            UserDictionaryToolFragment fragment = new UserDictionaryToolFragment();
            fm.beginTransaction().add(android.R.id.content, fragment).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.user_dictionary, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            case R.id.menu_add:
                TangoRegistDialog.newInstance().show(getSupportFragmentManager());
                break;

            case R.id.menu_delete:
                onMenuAction(new Action(ActionType.DELETE));
                break;

            case R.id.menu_revert:
                onMenuAction(new Action(ActionType.REVERT));
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onListWordClick(Word word, int position) {
        TangoRegistDialog.newInstance(word, position).show(getSupportFragmentManager());
    }

    private void onMenuAction(Action action) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(android.R.id.content);
        if (!(fragment instanceof UserDictionaryToolFragment)) {
            return;
        }
        UserDictionaryToolFragment udtf = (UserDictionaryToolFragment) fragment;
        udtf.onStartAction(action);
    }

    public static class TangoRegistDialog extends SherlockDialogFragment {

        public static final String TAG = "TangoRegistDialog";

        private static final String HIRAGANA = "^[\u3040-\u309f|\u30fc]+$";

        private static final String EXTRA_TANGO = "tango";
        private static final String EXTRA_YOMI = "yomi";
        private static final String EXTRA_POSITION = "position";

        public static TangoRegistDialog newInstance() {
            return newInstance(new Word(null, null), -1);
        }

        public static TangoRegistDialog newInstance(Word word, int position) {
            TangoRegistDialog dialog = new TangoRegistDialog();
            Bundle args = new Bundle();
            args.putString(EXTRA_TANGO, word.getTango());
            args.putString(EXTRA_YOMI, word.getYomi());
            args.putInt(EXTRA_POSITION, position);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            View view = View.inflate(getActivity(), R.layout.dialog_regist, null);
            EditText tangoView = (EditText) view.findViewById(R.id.edit_tango);
            EditText yomiView = (EditText) view.findViewById(R.id.edit_yomi);
            String tango = getArguments().getString(EXTRA_TANGO);
            String yomi = getArguments().getString(EXTRA_YOMI);
            tangoView.setText(tango);
            yomiView.setText(yomi);

            return new AlertDialog.Builder(getActivity())
                    .setTitle(R.string.dict_regist_title)
                    .setView(view)
                    .setPositiveButton(android.R.string.ok, null)
                    .setNeutralButton(android.R.string.cancel, null)
                    .create();
        }

        @Override
        public void onStart() {
            super.onStart();
            // キーボード表示
            getDialog().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);

            Button pButton = ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
            pButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onPositiveButtonClick(getDialog());
                }
            });
        }

        public void show(FragmentManager manager) {
            super.show(manager, TAG);
        }

        private void onPositiveButtonClick(Dialog dialog) {
            EditText tangoView = (EditText) dialog.findViewById(R.id.edit_tango);
            EditText yomiView = (EditText) dialog.findViewById(R.id.edit_yomi);
            String tango = tangoView.getText().toString();
            String yomi = yomiView.getText().toString();

            if (TextUtils.isEmpty(yomi)) {
                CukoApplication.showToast(R.string.dict_regist_error_yomi);
                return;
            }
            if (TextUtils.isEmpty(tango)) {
                CukoApplication.showToast(R.string.dict_regist_error_tango);
                return;
            }
            if (!Pattern.compile(HIRAGANA).matcher(yomi).matches()) {
                CukoApplication.showToast(R.string.dict_regist_error_hiragana);
                return;
            }
            if (!(getActivity() instanceof UserDictionaryToolActivity)) {
                return;
            }
            tangoRegist((UserDictionaryToolActivity) getActivity(), dialog);
            dialog.dismiss();
        }

        private void tangoRegist(UserDictionaryToolActivity activity, Dialog dialog) {
            EditText tangoView = (EditText) dialog.findViewById(R.id.edit_tango);
            EditText yomiView = (EditText) dialog.findViewById(R.id.edit_yomi);
            String tango = tangoView.getText().toString();
            String yomi = yomiView.getText().toString();
            int position = getArguments().getInt(EXTRA_POSITION, -1);

            if (position != -1) {
                activity.onMenuAction(new Action(ActionType.UPDATE, position, tango, yomi));
            } else {
                activity.onMenuAction(new Action(ActionType.ADD, tango, yomi));
            }
        }
    }
}
