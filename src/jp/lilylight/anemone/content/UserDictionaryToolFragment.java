/*
 * Cuko-plugin for twicca
 * $Id: UserDictionaryToolFragment.java,v 1.0 2013/07/04 13:18:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.content;

import static jp.lilylight.anemone.CukoApplication.getDictionary;
import static jp.lilylight.anemone.CukoApplication.getDictionaryName;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import jp.lilylight.amaryllis.LogD;
import jp.lilylight.anemone.CukoApplication;
import jp.lilylight.anemone.R;
import jp.lilylight.anemone.type.ActionType;
import jp.lilylight.anemone.util.ResourceListAdapter;
import jp.lilylight.anemone.util.SimpleAsyncTaskLoader;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;

public class UserDictionaryToolFragment extends SherlockListFragment implements
        LoaderCallbacks<List<Word>> {

    private ListView mListView;
    private WordAdapter mAdapter;
    private UserDictionaryToolListener mListener;
    private LinkedList<List<Action>> mActionStack = new LinkedList<List<Action>>();

    public static class Action extends Word {
        private ActionType mType;
        private int mLocation;

        public Action(ActionType type) {
            this(type, -1, null, null);
        }

        public Action(ActionType type, String tango, String yomi) {
            this(type, -1, tango, yomi);
        }

        public Action(ActionType type, int location, Word word) {
            this(type, location, word.getTango(), word.getValue());
        }

        public Action(ActionType type, int location, String tango, String yomi) {
            super(tango, yomi);
            mType = type;
            mLocation = location;
        }

        public ActionType getType() {
            return mType;
        }

        public void setType(ActionType type) {
            mType = type;
        }

        public int getLocation() {
            return mLocation;
        }

        public void setLocation(int location) {
            mLocation = location;
        }
    }

    public interface UserDictionaryToolListener {
        void onListWordClick(Word word, int position);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof UserDictionaryToolListener) {
            mListener = (UserDictionaryToolListener) getActivity();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mAdapter = new WordAdapter(getActivity(), R.layout.word, null);
        mListView = getListView();
        mListView.setAdapter(mAdapter);
        mListView.setFastScrollEnabled(true);

        // Start out with a progress indicator.
        setListShown(false);

        // Prepare the loader. Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!mActionStack.isEmpty()) {
            mActionStack.clear();
            Intent intent = new Intent(getActivity(), UserDictionarySaveService.class);
            getActivity().startService(intent);
        }
    }

    @Override
    public Loader<List<Word>> onCreateLoader(int id, Bundle args) {
        return new SimpleAsyncTaskLoader<List<Word>>(getActivity()) {
            @Override
            public List<Word> loadInBackground() {
                LogD.d("loadInBackground: isEmpty=[" + getDictionary().isEmpty() + "]");
                try {
                    if (getDictionary().isEmpty() && mActionStack.isEmpty()) {
                        getDictionary().load(getDictionaryName());
                    }
                    return new ArrayList<Word>(getDictionary().getWordList());
                } catch (IOException e) {
                    LogD.w("Dictionary load failed.");
                    e.printStackTrace();
                    return null;
                }
            }
        };
    }

    @Override
    public void onLoadFinished(Loader<List<Word>> loader, List<Word> data) {
        LogD.d("Count:" + (data != null ? data.size() : 0));
        mAdapter.changeList(data);

        // The list should now be shown.
        if (isResumed()) {
            setListShown(true);
        } else {
            setListShownNoAnimation(true);
        }
    }

    @Override
    public void onLoaderReset(Loader<List<Word>> loader) {
        mAdapter.changeList(null);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        LogD.d("onListItemClick: position=[" + position + "]");
        if (mListener != null) {
            mListener.onListWordClick(getDictionary().getWord(position), position);
        }
    }

    public boolean onStartAction(Action action) {
        return internalStartAction(action, false);
    }

    private boolean internalStartAction(Action action, boolean revert) {
        LogD.d("onMenuAction: action=[" + action.getType() + "]");
        switch (action.getType()) {
            case ADD:
                int index = getDictionary().indexOf(action.getTango());
                if (index != -1 && !revert) {
                    // すでにある単語は上書き
                    action.setType(ActionType.UPDATE);
                    action.setLocation(index);
                    return internalStartAction(action, false);
                } else if (action.getLocation() < 0) {
                    getDictionary().addWord(action);
                } else {
                    getDictionary().addWord(action.getLocation(), action);
                }
                if (!revert) {
                    action.setType(ActionType.DELETE);
                    addSingleAction(action);
                }
                return true;

            case UPDATE:
                Word oldWord = getDictionary().getWord(action.getLocation());
                getDictionary().setWord(action.getLocation(), action);
                if (!revert) {
                    addSingleAction(new Action(ActionType.UPDATE, action.getLocation(), oldWord));
                }
                return true;

            case DELETE:
                if (!revert) {
                    List<Action> actions = mAdapter.deleteItems();
                    if (actions != null) {
                        mActionStack.addLast(actions);
                        getLoaderManager().restartLoader(0, null, this);
                        CukoApplication.showToast(R.string.dict_action_delete);
                    } else {
                        CukoApplication.showToast(R.string.dict_action_error_delete);
                        return false;
                    }
                } else {
                    getDictionary().removeWord(action);
                }
                return true;

            case REVERT:
                if (mActionStack.isEmpty()) {
                    CukoApplication.showToast(R.string.dict_action_error_revert);
                    return false;
                }
                for (Action a : mActionStack.removeLast()) {
                    internalStartAction(a, true);
                }
                getLoaderManager().restartLoader(0, null, this);
                CukoApplication.showToast(R.string.dict_action_revert);
                return true;

            default:
                return false;
        }
    }

    private void addSingleAction(Action action) {
        List<Action> actions = new LinkedList<Action>();
        actions.add(action);
        mActionStack.addLast(actions);
        getLoaderManager().restartLoader(0, null, this);
    }

    private static class WordAdapter extends ResourceListAdapter<Word>
            implements OnCheckedChangeListener {

        private final List<Integer> mCheckList = new ArrayList<Integer>();
        private final List<Word> mDeleteList = new LinkedList<Word>();

        public WordAdapter(Context context, int layout, List<Word> list) {
            super(context, layout, list);
        }

        @Override
        public void bindView(int position, View view, Context context, Word word) {
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.dict_check);
            TextView tangoView = (TextView) view.findViewById(R.id.dict_tango);
            TextView yomiView = (TextView) view.findViewById(R.id.dict_yomi);
            checkBox.setChecked(mCheckList.contains(position));
            checkBox.setTag(position);
            tangoView.setText(word.getTango());
            yomiView.setText(word.getYomi());
        }

        @Override
        public View newView(int position, Context context, Word item, ViewGroup parent) {
            View view = super.newView(position, context, item, parent);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.dict_check);
            checkBox.setOnCheckedChangeListener(this);
            return view;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            Integer position = (Integer) buttonView.getTag();
            if (isChecked) {
                mCheckList.add(position);
            } else {
                mCheckList.remove(position);
            }
        }

        public List<Action> deleteItems() {
            if (mCheckList.isEmpty()) {
                return null;
            }
            Collections.sort(mCheckList); // Sort in ascending order
            List<Action> actionList = new LinkedList<Action>();
            for (Integer position : mCheckList) {
                Word word = getDictionary().getWord(position);
                actionList.add(new Action(ActionType.ADD, position, word));
                mDeleteList.add(word);
            }
            for (Word word : mDeleteList) {
                getDictionary().removeWord(word);
            }
            mCheckList.clear();
            mDeleteList.clear();
            return actionList;
        }
    }

}
