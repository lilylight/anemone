/*
 * Cuko-plugin for twicca
 * $Id: CukoConverter.java,v 1.2 2013/06/06 19:58:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone;

import static jp.lilylight.anemone.CukoApplication.getDictionary;
import static jp.lilylight.anemone.CukoApplication.getDictionaryName;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import jp.lilylight.amaryllis.LogD;
import jp.lilylight.anemone.content.Word;

public class CukoConverter {

    private static final Comparator<Word> COMPARATOR = new Comparator<Word>() {
        public int compare(Word lhs, Word rhs) {
            return rhs.getTango().length() - lhs.getTango().length();
        }
    };

    public String doString(String string) {
        try {
            if (getDictionary().isEmpty()) {
                getDictionary().load(getDictionaryName());
            }
            List<Word> wordList = new LinkedList<Word>(getDictionary().getWordList());
            Collections.sort(wordList, COMPARATOR); // 文字数が多い順にソート
            for (Word word : wordList) {
                string = string.replace(word.getTango(), word.getYomi());
            }
        } catch (IOException e) {
            LogD.w("Dictionary load failed.");
            e.printStackTrace();
        }
        return string;
    }

}
