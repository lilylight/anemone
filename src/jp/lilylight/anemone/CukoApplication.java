/*
 * Cuko-plugin for twicca
 * $Id: CukoApplication.java,v 1.0 2013/06/10 20:00:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone;

import jp.lilylight.amaryllis.LogD;
import jp.lilylight.anemone.content.CukoDictionary;
import android.app.Application;
import android.content.Context;
import android.text.ClipboardManager;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class CukoApplication extends Application {

    private static CukoDictionary sDictionary = new CukoDictionary();
    private static Context sContext;
    private static String sDictionaryName;

    @Override
    public void onCreate() {
        super.onCreate();
        initSettings();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    private final void initSettings() {
        String home = getApplicationInfo().dataDir;
        System.setProperty("kakasi.home", home);
        System.setProperty("kakasi.kanwaDictionary", home + "/lib/libkanwadict.so");
        System.setProperty("kakasi.itaijiDictionary.path", home + "/lib/libitaijidict.so");
        System.setProperty("kakasi.itaijiDictionary.encoding", "UTF-8");

        sContext = getApplicationContext();
        sDictionaryName = getInternalFileName();
        LogD.d("DictionaryName:" + sDictionaryName);
    }

    private String getInternalFileName() {
        return getFilesDir().getAbsolutePath() + "/" + "cukodict";
    }

    @SuppressWarnings("unused")
    private String getExternalFileName() {
        if (getExternalFilesDir(null) == null) {
            return null;
        }
        return getExternalFilesDir(null).getAbsolutePath() + "/" + "cukodict";
    }

    public static CukoDictionary getDictionary() {
        return sDictionary;
    }

    public static String getDictionaryName() {
        return sDictionaryName;
    }

    public static void showToast(int resId) {
        showToast(sContext.getString(resId));
    }

    public static void showToast(String text) {
        Toast toast = Toast.makeText(sContext, text, Toast.LENGTH_SHORT);
        // TextView tv = (TextView)
        // toast.getView().findViewById(android.R.id.message);
        // tv.setTypeface(MenmaTypeface.create(mContext.getAssets()));
        toast.show();
    }

    public static void copyText(String text) {
        ((ClipboardManager) sContext.getSystemService(CLIPBOARD_SERVICE)).setText(text);
        showToast(sContext.getString(R.string.cuko_comp));
    }
}
