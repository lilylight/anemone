/*
 * Cuko-plugin for twicca
 * $Id: SettingsActivity.java,v 1.1 2012/10/25 18:51:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone;

import jp.lilylight.anemone.content.UserDictionaryToolActivity;
import jp.lilylight.anemone.type.DisplayType;
import jp.lilylight.anemone.type.TweetType;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class SettingsActivity extends Activity implements
        RadioGroup.OnCheckedChangeListener,
        CompoundButton.OnCheckedChangeListener {

    private SharedPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        Typeface font = MenmaTypeface.create(getAssets());

        // TextView
        ((TextView) findViewById(R.id.text_timeline)).setTypeface(font);
        ((TextView) findViewById(R.id.text_tweet)).setTypeface(font);
        ((TextView) findViewById(R.id.text_other)).setTypeface(font);
        ((TextView) findViewById(R.id.text_dict)).setTypeface(font);
        ((TextView) findViewById(R.id.text_request)).setTypeface(font);

        // ユーザ辞書
        findViewById(R.id.text_dict).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SettingsActivity.this, UserDictionaryToolActivity.class));
            }
        });

        // 読み方
        StringBuilder html = new StringBuilder();
        html.append("<a href=\"")
                .append(getString(R.string.setting_read_url))
                .append("\">")
                .append(getString(R.string.setting_read))
                .append("</a>");

        TextView tv = (TextView) findViewById(R.id.text_read);
        tv.setTypeface(font);
        tv.setText(Html.fromHtml(html.toString()));
        tv.setMovementMethod(LinkMovementMethod.getInstance());

        // RadioGroup,RadioButton
        findViewById(R.id.radio_group_timeline).setTag(DisplayType.TIMELINE.getKey());
        findViewById(R.id.radio_group_tweet).setTag(DisplayType.TWEET.getKey());
        setupRadioGroup(findViewById(R.id.radio_group_timeline), TweetType.TWEET.getId(), font);
        setupRadioGroup(findViewById(R.id.radio_group_tweet), TweetType.TWEET.getId(), font);
        ((RadioGroup) findViewById(R.id.radio_group_timeline)).setOnCheckedChangeListener(this);
        ((RadioGroup) findViewById(R.id.radio_group_tweet)).setOnCheckedChangeListener(this);

        // CheckBox
        findViewById(R.id.check_tag).setTag(DisplayType.HASHTAG.getKey());
        ((CheckBox) findViewById(R.id.check_tag)).setTypeface(font);
        boolean check = mPreferences.getBoolean(DisplayType.HASHTAG.getKey(), false);
        ((CheckBox) findViewById(R.id.check_tag)).setChecked(check);
        ((CheckBox) findViewById(R.id.check_tag)).setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int id = ((TweetType) findViewById(checkedId).getTag()).getId();
        mPreferences.edit().putInt((String) group.getTag(), id).commit();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        mPreferences.edit().putBoolean((String) buttonView.getTag(), isChecked).commit();
    }

    private void setupRadioGroup(View root, int defaultId, Typeface font) {
        ((RadioButton) root.findViewById(R.id.radio_tweet)).setTypeface(font);
        ((RadioButton) root.findViewById(R.id.radio_copy)).setTypeface(font);
        ((RadioButton) root.findViewById(R.id.radio_share)).setTypeface(font);

        root.findViewById(R.id.radio_tweet).setTag(TweetType.TWEET);
        root.findViewById(R.id.radio_copy).setTag(TweetType.COPY);
        root.findViewById(R.id.radio_share).setTag(TweetType.SHARE);

        int id = mPreferences.getInt((String) root.getTag(), defaultId);
        switch (TweetType.idOf(id)) {
            case TWEET:
                ((RadioButton) root.findViewById(R.id.radio_tweet)).setChecked(true);
                break;

            case COPY:
                ((RadioButton) root.findViewById(R.id.radio_copy)).setChecked(true);
                break;

            case SHARE:
                ((RadioButton) root.findViewById(R.id.radio_share)).setChecked(true);
                break;
        }
    }

}
