/*
 * Cuko-plugin for twicca
 * $Id: DisplayType.java,v 1.1 2012/10/24 16:25:00 rug Exp $
 * Copyright (C) 2012
 * Takuya Naraoka (lilylight.android@gmail.com)
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either versions 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with KAKASI, see the file COPYING.  If not, write to the Free
 * Software Foundation Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

package jp.lilylight.anemone.type;

public enum DisplayType {
    TIMELINE("timeline"),
    TWEET("tweet"),
    HASHTAG("hashtag");

    private String key;

    private DisplayType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}
